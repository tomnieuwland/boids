# Boids

Boids flocking algorithm visualization using two.js

## What is Boids?
Boids is an artificial life program that simulates the flocking behaviour of birds or fish.

https://en.wikipedia.org/wiki/Boids

## How do I Boid?
You can boid locally by running:
- `npm install`
- `npm run dev-server`
- Open `localhost:3000` in your browser

Or you can boid right now by going to https://tomnieuwland.gitlab.io/boids/
