import { Vector } from 'two.js/src/vector';
import { adjectives, animals, uniqueNamesGenerator } from 'unique-names-generator';
import { round } from '../utils/numbers';
import { BoidShapes } from './boidShapes';
import { Flock } from './flock';

export class Boid {
  flock: Flock;
  shapes: BoidShapes;
  velocity: Vector;
  name: string;

  /**
   * Creates an instance of Boid.
   * @param {number} x Initial x co-ordinates
   * @param {number} y Initial y co-ordinates
   * @param {Two} scene Two.js scene
   * @memberof Boid
   */
  constructor(x: number, y: number, flock: Flock) {
    this.name = uniqueNamesGenerator({
      dictionaries: [adjectives, animals],
      separator: '-'
    });
    this.flock = flock;
    this.velocity = new Vector();
    this.shapes = new BoidShapes(x, y, this.flock.scene, this.name);
  }

  /**
   * Removes boid from the scene
   *
   * @memberof Boid
   */
  destroy() {
    this.shapes.destroy();
  }

  tick() {
    let velocities: Vector[] = [];
    let neighbours = this.getNeighbours();

    velocities.push(this.calculateCohesionVelocity(neighbours));
    velocities.push(this.calculateSeperationVelocity(neighbours));
    velocities.push(this.calculateAlignmentVelocity(neighbours));

    velocities.forEach((velocity) => {
      this.velocity.add(velocity.x, velocity.y);
    });

    this.limitSpeed();
    this.limitSceneMargin();
    let position = this.shapes.getPosition().clone();
    let newPosition = position.add(this.velocity.x, this.velocity.y);
    this.shapes.setPosition(newPosition.x, newPosition.y);
  }

  /**
   * Gets the distance to another boid
   *
   * @param {Boid} boid
   * @return {*}  {number}
   * @memberof Boid
   */
  getDistance(boid: Boid): number {
    return this.shapes.getPosition().distanceTo(boid.shapes.getPosition());
  }

  /**
   * Returns an array of boids considered close to this boid in 2D space
   *
   * @param {Boid[]} flock
   * @return {*}
   * @memberof Boid
   */
  getNeighbours(): Boid[] {
    let neighbours: Boid[] = [];
    this.flock.boids.forEach((boid) => {
      if (this == boid) {
        return;
      }
      let distance = this.getDistance(boid);
      if (distance <= this.flock.neighbourDistance) {
        neighbours.push(boid);
      }
    });
    return neighbours;
  }

  /**
   * Calculates the velocity a boid should experience from the cohesion rule
   *
   * @param {Boid[]} neighbours
   * @return {*}  {Vector}
   * @memberof Boid
   */
  calculateCohesionVelocity(neighbours: Boid[]): Vector {
    let velocity = new Vector();
    if (neighbours.length == 0) {
      // Boid has no neighbours, therefore has no velocity gained from seperation rule
      return velocity;
    }

    // Calculate average velocity to neighbouring boids
    neighbours.forEach((neighbour) => {
      let nPos = neighbour.shapes.getPosition();
      velocity.add(nPos.x, nPos.y);
    });

    if (neighbours.length > 0) {
      velocity.divideScalar(neighbours.length);
    }

    // Subtract current x,y
    let pos = this.shapes.getPosition();
    velocity.subtract(pos.x, pos.y);

    // Apply cohesion factor
    velocity.multiplyScalar(this.flock.cohesionFactor);

    return velocity;
  }

  calculateSeperationVelocity(neighbours: Boid[]): Vector {
    let velocity = new Vector();
    if (neighbours.length == 0) {
      // Boid has no neighbours, therefore has no velocity gained from seperation rule
      return velocity;
    }

    neighbours.forEach((neighbour) => {
      if (this.getDistance(neighbour) <= this.flock.seperationDistance) {
        // If the neighbour is too close, calculate the velocity to get away from the neighbour
        let pos = this.shapes.getPosition().clone();
        let nPos = neighbour.shapes.getPosition();
        pos.subtract(nPos.x, nPos.y);
        velocity.add(pos.x, pos.y);
      }
    });

    // Apply seperation factor
    velocity.multiplyScalar(this.flock.seperationFactor);

    return velocity;
  }

  calculateAlignmentVelocity(neighbours: Boid[]): Vector {
    let velocity = new Vector();
    if (neighbours.length == 0) {
      // Boid has no neighbours, therefore has no velocity gained from alignment rule
      return velocity;
    }

    neighbours.forEach((neighbour) => {
      velocity.add(neighbour.velocity.x, neighbour.velocity.y);
    });

    if (neighbours.length > 0) {
      velocity.divideScalar(neighbours.length);
    }

    velocity.subtract(this.velocity.x, this.velocity.y);

    velocity.multiplyScalar(this.flock.seperationFactor);

    return velocity;
  }

  limitSpeed() {
    let speed = Math.sqrt(Math.pow(this.velocity.x, 2) + Math.pow(this.velocity.y, 2));
    if (speed > this.flock.maxSpeed) {
      this.velocity.x = (this.velocity.x / speed) * this.flock.maxSpeed;
      this.velocity.y = (this.velocity.y / speed) * this.flock.maxSpeed;
    }
  }

  limitSceneMargin() {
    let currPos = this.shapes.getPosition();

    let leftMargin = this.flock.sceneMargin;
    let topMargin = this.flock.sceneMargin;
    let rightMargin = this.flock.scene.width - this.flock.sceneMargin;
    let bottomMargin = this.flock.scene.height - this.flock.sceneMargin;

    // two.js has the top left corner set as (0,0), and the bottom right set to two positive ints i.e (1920, 1080)
    if (currPos.x < leftMargin) {
      this.velocity.x += this.flock.turnFactor;
    }
    if (currPos.x > rightMargin) {
      this.velocity.x -= this.flock.turnFactor;
    }
    if (currPos.y < topMargin) {
      this.velocity.y += this.flock.turnFactor;
    }
    if (currPos.y > bottomMargin) {
      this.velocity.y -= this.flock.turnFactor;
    }
  }
}
