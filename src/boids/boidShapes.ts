import Two from 'two.js';
import { Group } from 'two.js/src/group';
import { Shape } from 'two.js/src/shape';
import { Circle } from 'two.js/src/shapes/circle';
import { Text } from 'two.js/src/text';
import { Vector } from 'two.js/src/vector';
import { generateRGB, validateRGB } from '../utils/colours';

const BOID_SIZE = 8;
const BOID_ALPHA = 1;

const NAMEPLATE_OFFSET = 10;
const NAMEPLATE_ALPHA = 0.75;

/**
 * Class for handling all two.js logic for Boids
 *
 * @export
 * @class BoidShapes
 */
export class BoidShapes {
  group: Group;
  boid: Circle;
  nameplate: Text;

  constructor(x: number, y: number, scene: Two, name: string) {
    this.group = scene.makeGroup([]);

    // Initialise the boid
    this.boid = scene.makeCircle(0, 0, BOID_SIZE);
    this.group.add(this.boid);
    this.setBoidFill(generateRGB());

    // Initilise the nameplate
    this.nameplate = scene.makeText(name, 0, this.boid.radius + NAMEPLATE_OFFSET, {
      opacity: NAMEPLATE_ALPHA
    });
    this.group.add(this.nameplate);
    this.nameplate._visible = false;

    this.group.position.set(x, y);
  }

  /**
   * Sets the colour of the boid given an array of RGB compatible values
   *
   * @param {RGB_ARRAY} arr
   * @memberof BoidShapes
   */
  setBoidFill(arr: RGB_ARRAY) {
    // Creates a string that looks like "rgba(0,0,0,0)"
    this.boid.fill = `rgba(${arr.join(',')}, ${BOID_ALPHA})`;
    // Set the border of the shape to be a darker variant of the given colour
    this.boid.stroke = `rgba(${arr.map((val) => val * 0.5).join(',')}, ${BOID_ALPHA})`;
  }

  /**
   * Convenience function for getting the vector for where the boid currently is
   *
   * @return {*}  {Vector}
   * @memberof Boid
   */
  getPosition(): Vector {
    return this.group.position;
  }

  setPosition(x: number, y: number) {
    this.group.position.set(x, y);
  }

  /**
   * Destorys the Boid group and all its children in the two.js scene
   *
   * @memberof BoidShapes
   */
  destroy() {
    this.group.children.forEach((child: Shape) => {
      child.remove();
    });
  }
}
