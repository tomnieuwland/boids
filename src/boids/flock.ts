import Two from 'two.js';
import { Boid } from './boid';

const NEIGHBOUR_DISTANCE = 300;
const SEPERATION_DISTANCE = 30;
const COHESION_FACTOR = 0.0005;
const SEPERATION_FACTOR = 0.007;
const ALIGNMENT_FACTOR = 0.05;
const MAX_SPEED = 7;
const SCENE_MARGIN = 150;
const TURN_FACTOR = 0.1;

export class Flock {
  // Two.js stuff
  scene: Two;

  // Boids stuff
  boids: Boid[];
  cohesionFactor: number;
  seperationFactor: number;
  alignmentFactor: number;
  neighbourDistance: number;
  seperationDistance: number;
  maxSpeed: number;
  sceneMargin: number;
  turnFactor: number;

  constructor(scene: Two) {
    this.boids = [];
    this.scene = scene;
    this.cohesionFactor = COHESION_FACTOR;
    this.seperationFactor = SEPERATION_FACTOR;
    this.alignmentFactor = ALIGNMENT_FACTOR;
    this.neighbourDistance = NEIGHBOUR_DISTANCE;
    this.seperationDistance = SEPERATION_DISTANCE;
    this.maxSpeed = MAX_SPEED;
    this.sceneMargin = SCENE_MARGIN;
    this.turnFactor = TURN_FACTOR;
  }

  addBoid(x?: number, y?: number): Boid {
    if (!x) {
      x = Math.floor(Math.random() * this.scene.width);
    }

    if (!y) {
      y = Math.floor(Math.random() * this.scene.height);
    }

    let boid = new Boid(x, y, this);
    this.boids.push(boid);
    return boid;
  }

  tick() {
    this.boids.forEach((boid) => {
      boid.tick();
    });
  }
}
