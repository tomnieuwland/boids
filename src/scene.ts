import Two from 'two.js';
import { Flock } from './boids/flock';

const rootElement = document.getElementById('root');

if (!rootElement) {
  throw new Error('Could not locate root element...');
}

const scene = new Two({
  fullscreen: true
}).appendTo(rootElement);

let flock = new Flock(scene);

for (let i = 0; i < 100; i++) {
  flock.addBoid();
}

scene.bind('update', update);

scene.play();

function update(frameCount: number) {
  flock.tick();
}
