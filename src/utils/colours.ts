/**
 * Validates that all values in an RGB_ARRAY are inside the valid RGB value range of 0 <= x <= 255
 *
 * @param {RGB_ARRAY} arr
 * @return {*}  {boolean}
 */
export function validateRGB(arr: RGB_ARRAY): boolean {
  return arr.filter((value) => 0 <= value && value <= 255).length === 3;
}

/**
 * Generates a random RGB colour
 *
 * @export
 * @return {*}  {RGB_ARRAY}
 */
export function generateRGB(): RGB_ARRAY {
  return <RGB_ARRAY>Array.from({ length: 3 }, () => Math.floor(Math.random() * 256));
}
