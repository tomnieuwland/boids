var path = require('path');

module.exports = {
  mode: "development",
  devtool: "inline-source-map",
  entry: "./src/scene.ts",
  output: {
      path: path.resolve(__dirname, "public/js"),
      filename: "bundle.js"
  },
  resolve: {
      // Add `.ts` and `.tsx` as a resolvable extension.
      extensions: [".ts", ".tsx", ".js"]
  },
  module: {
      rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      { test: /\.tsx?$/, loader: "ts-loader" }
      ]
  },
  devServer: {
    static: {
      directory: path.join(__dirname, 'public'),
    },
    compress: true,
    port: 3000,
  },
};